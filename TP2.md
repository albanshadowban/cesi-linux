# TP 2

### Installation Mariadb

On utilise la commande ***sudo dnf install mariadb-server***



### Démarage du service Mariadb

Pour démarrer le service mariadb on utilise ***sudo systemctl start mariadb***.
Pour démarrer le service  mariadb au démarrage on utilise ***sudo systemctl enable mariadb***.
On vérifie l'état du service avec ***sudo systemctl status mariadb***
On a alors:

	● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-15 11:49:27 CET; 3s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 9155 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 9020 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 8996 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 9123 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11234)
   Memory: 82.4M
   CGroup: /system.slice/mariadb.service
           └─9123 /usr/libexec/mysqld --basedir=/usr
		   
		   
On vérifie sur quel port écoute le service mysql avec  ***sudo ss -luptn***.

tcp                      LISTEN                    0                         80                                                       *:3306                                                    *:*                        users:(("mysqld",pid=9123,fd=21))

Il est sur le port 80.

### Firewall
Pour ouvrir le port 3306 ***sudo firewall-cmd --permanant --add-port=3306***

### Configuration Mysql

On exécute la commande ***mysql_secure_installation***
mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] yes
New password:
Re-enter new password:
Sorry, passwords do not match.

New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] n
 ... skipping.

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!

### Préparation BDD
Pour se connecter a la BDD avec mot de passe on utilise ***sudo mysql -u root -p***.
***CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';*** cette ligne permet de créer l'utilisateur 'nextcloud' avec le mdp 'meow' (a faire)

Création de la base de donnée qui sera utilisée par NextCloud :
**CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;**

On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
**GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';**

Actualisation des privilèges
**FLUSH PRIVILEGES;**

**dnf provides mysql** permet de trouver le paquet incluant la commande
Ensuite depuis web.tp2.cesi on se connecte a la bdd.
**mysql -h 10.2.1.12 -p -u nextcloud nextcloud**
Enter password:

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show tables;
Empty set (0.00 sec)

La connection a la bdd fonctionne


### APACHE
Installation
**sudo dnf install httpd -y**

Lancer le service.
